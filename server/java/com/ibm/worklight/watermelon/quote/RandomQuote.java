package com.ibm.worklight.watermelon.quote;

/**
 * This class generate random quote
 * 
 * @author Ali Irawan (boylevantz@gmail.com)
 *
 */
public class RandomQuote {

	private static String[] quotes = {"IBM Worklight is cool!",
			           "Extends your business to mobile",
			           "Support multiple mobile operating environments and devices",
			           "Safeguard mobile security",
			           "Connect and synchronize",
			           "Build awesome hybrid application"
	};
	
	/**
	 * Get random quote
	 * @return quote string
	 */
	public static String getRandomQuote(){
		int count = quotes.length;
		int randomInt = (int)(Math.random()*count);
		return quotes[randomInt];
	}
}
