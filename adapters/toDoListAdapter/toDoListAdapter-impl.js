/*
 *  Licensed Materials - Property of IBM
 *  5725-G92 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/*******************************************************************************
 * Functions that correspond to JSONStore client operations
 * 
 */

var selectStatement = WL.Server.createSQLStatement("select id, task, description, due_date from `todo`");

function getToDoListAdapters() {
		
	return WL.Server.invokeSQLStatement({
		preparedStatement : selectStatement,
		parameters : []
	});
}

var addStatement = WL.Server.createSQLStatement("insert into `todo` (task, description,due_date) values (?,?,?)");

function addToDoListAdapter(task,description,due_date) {
		
	return WL.Server.invokeSQLStatement({
		preparedStatement : addStatement,
		parameters : [task, description, due_date]
	});
}
	
var updateStatement = WL.Server.createSQLStatement("update `todo` set task=?, description=?, due_date=? where id=?");

function updateToDoListAdapter(id,task,description,due_date) {
		
	return WL.Server.invokeSQLStatement({
		preparedStatement : updateStatement,
		parameters : [task,description,due_date,id]
	});
}

var deleteStatement = WL.Server.createSQLStatement("delete from `todo` where id=?");

function deleteToDoListAdapter(id) {
		
	return WL.Server.invokeSQLStatement({
		preparedStatement : deleteStatement,
		parameters : [id]
	});
}