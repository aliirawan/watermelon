/*
 *  Licensed Materials - Property of IBM
 *  5725-G92 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */
/**
 * Get quote of the day
 * @returns Quote of the day
 */
function getQuote() {
	
	// Call the static method
	
	return {
		quote: com.ibm.worklight.watermelon.quote.RandomQuote.getRandomQuote()
	};
}