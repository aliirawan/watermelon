/*
 *  Licensed Materials - Property of IBM
 *  5725-G92 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/**
 * Get Google map Latitude/Longitude by address
 * @param pAddress address
 */
function getGmapLatLng(pAddress) {

    var input = {
        method : 'get',
        returnedContentType : 'json',
        path : 'maps/api/geocode/json',
        parameters : {
            'address' : pAddress,
            'sensor' : 'false'   // hard-coded
        }
    };
    
    var response = WL.Server.invokeHttp(input);
    var type = typeof response; 
    if ("object" == type) {
        if (true == response["isSuccessful"]) {
            
            // Drill down into the response object.
            var results = response["results"];
            var result = results[0];
            var geometry = result["geometry"];
            var location = geometry["location"];
            
            // Return JSON object with lat and lng.
            return location;
        } 
        else {
            // Returning null. Web request was not successful.
            return null;
        }
    } 
    else {
        // Returning null. Response is not an object.
        return null;
    }
}

