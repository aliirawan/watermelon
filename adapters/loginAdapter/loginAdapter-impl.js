/*
 *  Licensed Materials - Property of IBM
 *  5725-G92 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/************************************************************************
 * Implementation code for procedure - 'procedure1'
 *
 *
 * @return - invocationResult
 */
 
var procedure1Statement = WL.Server.createSQLStatement("select * from users where username = ? and password = MD5(?)");
function authenticate(username,password) {
	var response = WL.Server.invokeSQLStatement({
		preparedStatement : procedure1Statement,
		parameters : [username,password]
	});
	
	if(response.resultSet.length==0){
		// Login invalid
		return { status: 'FAIL' };
	}
	return { status: 'OK', 
		     user: {
		    	 id: response.resultSet[0].id
		     } 
	};
}


