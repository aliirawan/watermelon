/**
* @license
* Licensed Materials - Property of IBM
* 5725-G92 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
* US Government Users Restricted Rights - Use, duplication or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/
var WATERMELON = {};
WATERMELON.todoList = [];

function wlCommonInit(){

	/*
	 * Application is started in offline mode as defined by a connectOnStartup property in initOptions.js file.
	 * In order to begin communicating with Worklight Server you need to either:
	 * 
	 * 1. Change connectOnStartup property in initOptions.js to true. 
	 *    This will make Worklight framework automatically attempt to connect to Worklight Server as a part of application start-up.
	 *    Keep in mind - this may increase application start-up time.
	 *    
	 * 2. Use WL.Client.connect() API once connectivity to a Worklight Server is required. 
	 *    This API needs to be called only once, before any other WL.Client methods that communicate with the Worklight Server.
	 *    Don't forget to specify and implement onSuccess and onFailure callback functions for WL.Client.connect(), e.g:
	 *    
	 *    WL.Client.connect({
	 *    		onSuccess: onConnectSuccess,
	 *    		onFailure: onConnectFailure
	 *    });
	 *     
	 */$('[data-role=navbar] a').on('click', function() { $(this).removeClass('ui-btn-active'); });
	
	
	// Common initialization code goes here
	
	// Add handler for button Register
    $('#btnRegister').click(function(){
    	alert('Do register here!');
    });
    
    // Add handler for button Login
    $('#btnLogin').click(function(){
    	var email = $('#email').val();
    	var password = $('#password').val();
    	
    	if(email==''){
    		alert('Email is required');
    		return;
    	}
    	if(password==''){
    		alert('Password is required');
    		return;
    	}
    	
    	// Do login here
    	var invocationData = {
    		adapter: 'loginAdapter',
    		procedure: 'authenticate',
    		parameters: [email,password]
    	};
    	WL.Client.invokeProcedure(invocationData,{
    		onSuccess: function(response){
	    		if(response.invocationResult.isSuccessful){
	    			if(response.invocationResult.status=='OK'){
	    		    	showTodoList();
	    			}else{
	    				alert('Email and password not match');
	    			}
	    		}
    		},
    		onFailure: function(response){
    			alert(response);
    		}
    	});
    });

}

/**
 * Call the adapters todoListAdapter, and render the result in a list view
 */
function loadTodoList(){

	// Clear todo
    $('#todo').html('');
	$.mobile.showPageLoadingMsg();
	var invocationData = {
    		adapter: 'toDoListAdapter',
    		procedure: 'getToDoListAdapters',
    		parameters: []
	};
	WL.Client.invokeProcedure(invocationData,{
		onSuccess: function(response){
    		if(response.invocationResult.isSuccessful){
    			var data = response.invocationResult.resultSet;
    			 
    			var html = '';
    			WATERMELON.todoList = [];
    			$.each(data, function(index,item){
    				html += '<li><a href="#" data-type="listitem" data-id="'+item.id+'">' + item.task + '</a></li>';
    				WATERMELON.todoList.push(item);
    				$('[data-role=navbar] a').on('click', function() { $(this).removeClass('ui-btn-active'); });});
    			WL.Logger.debug(WATERMELON.todoList);
    			$('#todo').html(html);
    			$('#todo').listview('refresh');
    			// Add handler when the list item is clicked
    			$('a[data-type=listitem]').click(function(o){
    				var id = $(this).attr('data-id');
    				showTaskDetail(id);
    			});
    			$.mobile.hidePageLoadingMsg();
    		}
		},
		onFailure: function(response){
			alert(response);
		}
	});
}
/**
 * Call the quoteAdapter and render the result to a single div
 */
function loadRandomQuote(){
    $('#randomQuoteContent').html('');
	$.mobile.showPageLoadingMsg();
	var invocationData = {
    		adapter: 'quoteAdapter',
    		procedure: 'getQuote',
    		parameters: []
	};
	WL.Client.invokeProcedure(invocationData,{
		onSuccess: function(response){
    		if(response.invocationResult.isSuccessful){
    			var data = response.invocationResult.quote;
    			$('#randomQuoteContent').html(data);
    			$.mobile.hidePageLoadingMsg();
    		}
		},
		onFailure: function(response){
			alert(response);
		}
	});
}
/**
 * Show the task detail by specified ID
 * @param id task ID to be shown
 */
function showTaskDetail(id){
	
	var taskIndex = -1;
	if(WATERMELON.todoList==undefined||WATERMELON.todoList==null) return;
	$.each(WATERMELON.todoList, function(index,item){
		WL.Logger.debug("Result: " + (item.id == id));
		if(item.id == id){
			taskIndex = index;
		}
	});	
	WL.Logger.debug("Index: " + taskIndex);
	
	var details = WATERMELON.todoList[taskIndex];
	WL.Logger.debug("Details: " + details);
	
	if(details==undefined){
		alert('Cannot find specified task');
	}else{
		$('#taskDetailContent').find('#task').html(details.task);
		$('#taskDetailContent').find('#description').html(details.description);
		$('#taskDetailContent').find('#dueDate').html(details.due_date);
		$.mobile.changePage('#taskDetail');
	}
}
function getLatLng(){
	// Call myRESTAdapter
	$('#latitude').html('Retrieving...');
	$('#longitude').html('Retrieving...');
	$.mobile.showPageLoadingMsg();
	var invocationData = {
    		adapter: 'myRESTAdapter',
    		procedure: 'getGmapLatLng',
    		parameters: [$('#address').val()]
	};
	WL.Client.invokeProcedure(invocationData,{
		onSuccess: function(response){
    		if(response.invocationResult.isSuccessful){
    			var lat = response.invocationResult.lat;
    			var lng = response.invocationResult.lng;
    			$('#latitude').html(lat);
    			$('#longitude').html(lng);
    			$.mobile.hidePageLoadingMsg();
    		}
		},
		onFailure: function(response){
		    $('#latitude').html('-');
			$('#longitude').html('-');
			$.mobile.hidePageLoadingMsg();
			alert(response);
		}
	});
}
function findMyLoc(){
	// More info using Geolocation
	// https://developer.mozilla.org/en-US/docs/Web/API/Geolocation.getCurrentPosition
	
	if(navigator.geolocation==undefined){
		alert('Geolocation is not supported in this device');
		return;
	}
    $('#latitude').html('Retrieving...');
	$('#longitude').html('Retrieving...');
	navigator.geolocation.getCurrentPosition(
			 // When success
			 function(position){
				 var crd = position.coords;
				 $('#latitude').html(crd.latitude);
				 $('#longitude').html(crd.longitude);
			 }, 
			 // When error (Optional)
			 function(error){
				 // Hide show in map button
 			     $('#latitude').html('-');
				 $('#longitude').html('-');
				 
				 switch(error.code) 
				    {
					    case error.PERMISSION_DENIED:
					      $('#err_message').html("User denied the request for Geolocation.");
					      break;
					    case error.POSITION_UNAVAILABLE:
 				    	  $('#err_message').html("Location information is unavailable.");
					      break;
					    case error.TIMEOUT:
					      $('#err_message').html("The request to get user location timed out.");
					      break;
					    case error.UNKNOWN_ERROR:
				    	  $('#err_message').html("An unknown error occurred.");
					      break;
				    }
			 });
			 // Positions Object (Optional)
			 // https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions
			 
}
function showInMap(){
	
	// This function assume that latitude and longitude is must already available
	var lat = $('#latitude').html();
	var lng = $('#longitude').html();
	
	if(lat=='-' || lng=='-'){
		alert('Position is not available');
		return;
	}
	
	var centerPos = new google.maps.LatLng(lat, lng); 
	$.mobile.changePage('#map');
	var mapOptions = {
	    center: centerPos,
	    zoom: 8
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"),  mapOptions);
	// To add the marker to the map, use the 'map' property
	var marker = new google.maps.Marker({
	    position: centerPos,
	    map: map,
	    title:"I'm here",
	    animation: google.maps.Animation.BOUNCE
	});
}
function addTodo(){
	WL.Logger.debug("addTodo()");
	
	var subject = $('#addtodo').find('#subject').val();
	var description = $('#addtodo').find('#description').val();
	var dueDate = $('#addtodo').find('#dueDate').val();
	
	WL.Logger.debug("Subject: " + subject);
	WL.Logger.debug("Description: " + description);
	WL.Logger.debug("Due date: " + dueDate);
	
	// Call adapters
	$.mobile.showPageLoadingMsg();
	var invocationData = {
    		adapter: 'todoListAdapter',
    		procedure: 'addTodoListAdapter',
    		parameters: [subject, description, dueDate]
	};
	WL.Client.invokeProcedure(invocationData,{
		onSuccess: function(response){
    		if(response.invocationResult.isSuccessful){
    			var errors = response.invocationResult.errors;
    			if(errors.length >0){
    				// There are error(s)
    				alert(errors[0]);
    			}
    			$.mobile.hidePageLoadingMsg();
    		}
		},
		onFailure: function(response){
			$.mobile.hidePageLoadingMsg();
			alert(response);
		}
	});
}
function showTodoList(){
	loadTodoList();
	$.mobile.changePage('#todoList');
	$('a[menu=todo]').addClass('ui-btn-active');
	$('a[menu=quote]').removeClass('ui-btn-active');
	$('a[menu=geocode]').removeClass('ui-btn-active');
}
function showRandomQuote(){
	loadRandomQuote();
	$.mobile.changePage('#randomQuote');
	$('a[menu=todo]').removeClass('ui-btn-active');
	$('a[menu=quote]').addClass('ui-btn-active');
	$('a[menu=geocode]').removeClass('ui-btn-active');
}
function showGeocode(){
	$.mobile.changePage('#geocode');
	$('a[menu=todo]').removeClass('ui-btn-active');
	$('a[menu=quote]').removeClass('ui-btn-active');
	$('a[menu=geocode]').addClass('ui-btn-active');
}
function signOut(){
	$('div#login').find('#email').val('');
	$('div#login').find('#password').val('');
	$.mobile.changePage('#login');
}
/*************************
 * HELPERS
 *************************/
function updateMenu(menuName){
	// $('a[date-role=navbar] a')
}